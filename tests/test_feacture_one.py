from src.generate_movies import generate_all_movies, generate_by_letter
from src.filter_tag import generate_movies_by_tag


def test_generate_all_movies_len():
    total_records = 9742
    input_dict = {
        'title': 'all',
        'order': 'desc',
        'by': 'Title'
    }
    movies = generate_all_movies(input_dict)
    assert movies is not None, 'movies is empty'
    assert total_records == len(movies), 'the number of records are not equal'


def test_order_by_movie_id_asc():
    input_dict = {
        'title': 'all',
        'order': 'asc',
        'by': 'Release'
    }
    last_expected = {
        "MovieId": "99992",
        "Title": "Shadow Dancer",
        "Release": "2012",
        "Genres": [
            "Crime",
            "Drama",
            "Thriller"
        ]
    }

    movies = generate_all_movies(input_dict)
    assert movies is not None, 'movies is empty'
    assert last_expected in movies, 'the test value is not in the array of dict'


def test_by_firt_letter():
    input_dict = {
        'title': 'T*',
        'genre': 'Animation'
    }
    expected_dict = {
        "MovieId": "182639",
        "Title": "The Second Renaissance Part II",
        "Release": "2003",
        "Genres": [
            "Animation",
            "Sci-Fi"
        ]
    }

    movies = generate_by_letter(input_dict)
    assert movies is not None, 'movies is empty'
    assert expected_dict in movies, 'the test value is not in the array of dict'
