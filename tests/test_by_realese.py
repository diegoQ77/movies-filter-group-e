import imp
from src.filter_rating import generate_json_of_rating, generate_json_of_rating_user


def test_by_rating_number():
    input_dict = {
        'rating': 3.0,
    }
    expected_dict = {
        "Title": "Hazard",
        "Release": "2005",
        "Genres": [
            "Action",
            "Drama",
            "Thriller"
        ],
        "ratings": [
            {
                "rating": "3.0",
                "date_time": "Wednesday, May 03, 2017 04:53:14 PM"
            }
        ]
    }

    movies = generate_json_of_rating(input_dict)
    assert movies is not None, 'movies is empty'
    assert expected_dict in movies, 'the test value is not in the array of dict'


def test_by_rating_number_and_user():
    input_dict = {
        'rating': 'all',
        'user': 3
    }
    expected_dict = {
        "Title": "2012",
        "Release": "2009",
        "Genres": [
            "Action",
            "Drama",
            "Sci-Fi",
            "Thriller"
        ],
        "ratings": [
            {
                "rating": "0.5",
                "date_time": "Thursday, May 26, 2011 10:42:44 PM"
            }
        ]
    }

    movies = generate_json_of_rating_user(input_dict)
    assert movies is not None, 'movies is empty'
    assert expected_dict in movies, 'the test value is not in the array of dict'
