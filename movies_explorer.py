from src.options_arguments import handle_options


def main():
    handle_options()


if __name__ == "__main__":
    main()
