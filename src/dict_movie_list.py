from src.file_reader import file_reader


def built_array_movies():
    movies = file_reader('movies')
    order_movies = []
    for line in movies:
        order_movies.append(get_information_for_movie(line))
    return order_movies


def get_information_for_movie(line):
    return {'MovieId': obtein_movieId(line), 'Title': obtein_title(line), 'Release': obtein_release(line), 'Genres': obtein_genre(line)}


def obtein_movieId(line):
    get_movieId = line['movieId'].split(' ')
    return get_movieId[0]


def obtein_release(line):
    get_title = line['title'].split(' ')
    a = get_title[-1].strip('()')
    return a


def obtein_title(line):
    get_title = line['title'].split(' ')
    return ' '.join(get_title[:len(get_title) - 1])


def obtein_genre(line):
    genres = [genre for genre in line['genres'].split("|")]
    return genres
