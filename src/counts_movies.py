
import json
from src.dict_movie_list import built_array_movies

def save_counts_movies_by_gen():
    order_movies_list = built_array_movies()
    return counts_movies_by_gen(order_movies_list)
     

def counts_movies_by_gen(movies_list: list) -> list : # Receive a list a return a list of dictionaries
    if (len(movies_list)) == 0:
        return [{}]
    
    all_genres = [] # create a empty list
    for movies in movies_list: # for each movien in list of movies
        genres = movies.get("Genres") # get the value of genres and split it in a list
        all_genres.extend(genres) # Extend the list

            
    set_datamovies = set(all_genres) # Delete the duplicate values

    genres_movies_count_list = [] # create a empty list
    for genre in set_datamovies: # Iterate through the set set of set_datamovies
        data_counter = {} # create a dictionaty for each iteration
        data_counter['genres'] = genre # Assigned the name of the genero 
        data_counter['count'] = all_genres.count(genre) # Counts in the genres_list how may time the value show up.
        genres_movies_count_list.append(data_counter) # and finally we save the dict with the information in genres_movies_count_list 

    return genres_movies_count_list   



