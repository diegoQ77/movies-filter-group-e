from src.file_reader import file_reader
from src.dict_movie_list import built_array_movies


def generate_all_movies(dict_args):
    # Get data from file
    list_movies = []
    # Default values
    ascendent = False
    sorted_by = 'Title'
    movies_list = built_array_movies()
    if (dict_args['order'] is not None and (dict_args['order'] == 'asc' or dict_args['order'] == 'ascending')):
        ascendent = True
        if (dict_args['by'] is not None):
            sorted_by = dict_args['by']
    if (dict_args['order'] is not None and (dict_args['order'] == 'desc' or dict_args['order'] == 'descendant')):
        if (dict_args['by'] is not None):
            sorted_by = dict_args['by']

    list_movies = handle_movie_order(movies_list, ascendent, sorted_by)

    return list_movies


def generate_by_letter(dict_args):
    order_movies_list = built_array_movies()
    movie_name = dict_args['title']
    movie_genre = dict_args['genre']
    print(movie_genre)
    list_of_movies_by_letter = []
    for line in order_movies_list:
        if (line['Title'] != ''):
            if (line['Title'][0] == movie_name[0] and movie_genre in line['Genres']):
                list_of_movies_by_letter.append(line)
    return list_of_movies_by_letter


def handle_movie_order(movies_list, order, sorted_by='Title'):
    movies_list.sort(
        key=lambda title: title.get(sorted_by), reverse=order)
    return movies_list
