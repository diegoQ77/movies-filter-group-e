from src.file_reader import file_reader
from src.dict_movie_list import built_array_movies
from datetime import datetime


def generate_json_of_rating(dict_args):
    ratings = filter_rating('ratings', dict_args['rating'])
    array_of_movies_ratings = remove_movie_id_from_list(
        built_movie_dict(ratings)
    )
    return array_of_movies_ratings


def generate_json_of_rating_user(dict_args):
    ratings = filter_rating_by_user(
        'ratings', dict_args['rating'], dict_args['user'])
    array_of_movies_ratings_by_user = remove_movie_id_from_list(
        built_movie_dict(ratings)
    )
    return array_of_movies_ratings_by_user


def filter_rating(file_name, rating_argument):
    ratins = file_reader(file_name)
    result = []
    for rating in ratins:
        if float(rating['rating']) == float(rating_argument):
            result.append(built_rating_dict(rating))
    return result


def filter_rating_by_user(file_name, rating_parameter, user_id):
    ratins = file_reader(file_name)
    result = []
    for rating in ratins:
        if (rating_parameter == 'all'):
            if int(rating['userId']) == int(user_id):
                result.append(built_rating_dict(rating))
        elif float(rating['rating']) == float(rating_parameter):
            if int(rating['userId']) == int(user_id):
                result.append(built_rating_dict(rating))
    return result


def built_movie_dict(ratings):
    movies = built_array_movies()
    list_of_movies = []
    for rating in ratings:
        rating_list = []
        for movie in movies:
            if rating['MovieId'] == movie['MovieId']:
                del rating['MovieId']
                rating_list.append(rating)
                movie['ratings'] = rating_list
                list_of_movies.append(movie)
                break
    return list_of_movies


def remove_movie_id_from_list(list_movies):
    data = []
    for line in list_movies:
        if 'MovieId' in line:
            del line['MovieId']
            data.append(line)
    return data


def built_rating_dict(rating_line):
    return {'MovieId': rating_line['movieId'], 'rating': rating_line['rating'], 'date_time': datetime.fromtimestamp(
        int(rating_line['timestamp'])).strftime('%A, %B %d, %Y %I:%M:%S %p')}
