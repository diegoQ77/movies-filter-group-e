import csv


def file_reader(file_name=''):
    document = f'data/{file_name}.csv'

    results = []
    with open(document, encoding='UTF-8') as file:
        reader = csv.DictReader(
            file, quoting=csv.QUOTE_ALL, quotechar='"')
        for row in reader:
            results.append(row)
    return results
