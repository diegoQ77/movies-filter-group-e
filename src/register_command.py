from csv import writer


def register_command(uid, command_line, date_now):
    list_data = [uid, command_line, date_now]

    with open('output/request.csv', 'a', newline='') as file:
        writer_object = writer(file)
        writer_object.writerow(list_data)
