from src.counts_movies import save_counts_movies_by_gen
import argparse
import hashlib
import json
from src.filter_tag import generate_movies_by_tag
from src.register_command import register_command
from datetime import datetime
from src.generate_movies import generate_all_movies, generate_by_letter
from src.format_data import format_data
from src.release_date import release_date_of_movie
from src.filter_rating import generate_json_of_rating, generate_json_of_rating_user


def manage_arguments_functions(dict_arg, uid):
    json_response = []
    if (dict_arg['title'] is not None):
        if (dict_arg['title'] == 'all'):
            print('entro if all')
            json_response = generate_all_movies(dict_arg)
        elif (dict_arg['title'][-1] == '*'):
            json_response = generate_by_letter(dict_arg)
        elif (dict_arg['date'] == 'All'):
            json_response = release_date_of_movie(dict_arg['title'])
        elif (dict_arg['tags'] is not None):
            json_response = generate_movies_by_tag(dict_arg['title'])
        else:
            print("The value of argument is not valid")
    elif (dict_arg['rating'] is not None and dict_arg['user'] is not None):
        json_response = generate_json_of_rating_user(dict_arg)

    elif (dict_arg['rating'] is not None):
        json_response = generate_json_of_rating(dict_arg)
    elif (dict_arg['genre'] == 'all'):
        json_response = save_counts_movies_by_gen()
    else:
        print("The value of argument is not valid")

    if json_response:
        final_response = format_data(uid, json_response)
        pretty_print_json(final_response)
    else:
        print("The value of argument is not register into csvs")


def pretty_print_json(json_data):
    print(json.dumps(json_data, indent=4))
    pass


def handle_options_command(input_dict_values):
    if (input_dict_values):
        command_user = parse_dict_to_string(
            built_dict_f_user_input(input_dict_values))
        date_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        uid = hashlib.sha256(str.encode(date_now)).hexdigest()
        register_command(uid, command_user, date_now)
        manage_arguments_functions(input_dict_values, uid)

    else:
        print(
            "Please enter some arguments and values, for mor information type -h or --help")


def parse_dict_to_string(input_dict):
    command_user = ""
    for key, value in input_dict.items():
        command_user = command_user + f"--{key} {value} "
    return command_user


def handle_arguments_command():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--title", help="Allows you to select if you want to use all movies.")
    parser.add_argument(
        "--order", help="You can choose order ascending or descending.")
    parser.add_argument(
        "--by", help="Allows you to choose the arguments under which you want to filter the content.")
    parser.add_argument(
        "--genre", help="Allows you to filter content according to genre.")
    parser.add_argument(
        "--date", help="Select the year to search for a specific date or 1 for dates.")
    parser.add_argument(
        "--tags", help="Specify a special tag to search the movie")
    parser.add_argument(
        "--rating", help="Specify a specific rating to perform the search")
    parser.add_argument(
        "--counters", help="Activate the counter to find out how many movies are in your search")
    parser.add_argument(
        "--user", help="Search ratings made by users")

    args = parser.parse_args()
    dict_values = vars(args)
    return dict_values


def built_dict_f_user_input(dict_values):
    return {key: value for key,
            value in dict_values.items() if value is not None}


def handle_options():
    input_args = handle_arguments_command()
    handle_options_command(input_args)
