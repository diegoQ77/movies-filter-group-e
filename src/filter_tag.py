from src.file_reader import file_reader
from src.dict_movie_list import built_array_movies
from datetime import datetime


def filter_tag(file_name, movieId):
    tags_list = file_reader(file_name)
    result = []
    for tag in tags_list:
        if tag['movieId'] == movieId:
            result.append({'tag': tag['tag'], 'date_time': datetime.fromtimestamp(
                int(tag['timestamp'])).strftime('%A, %B %d, %Y %I:%M:%S %p')})
    return result


def search_movie(title):
    all_movies = built_array_movies()
    return ([movie for movie in all_movies if movie['Title'] == title][0])


def generate_movies_by_tag(title):
    movie = search_movie(title)
    tags = filter_tag('tags', movie['MovieId'])
    movie['tags'] = tags
    list_tag = []
    list_tag.append(movie)
    return list_tag
