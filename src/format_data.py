import json


def format_data(identification: str, lista_movies: list) -> json:
    id_respose = identification  # receive the Identification
    size = len(lista_movies)  # get the len of the list
    # open the file and save the information in file
    with open(f"output/{id_respose}.json", "w") as file:
        json.dump({'request': id_respose, 'data': lista_movies, 'size': size},
                  file, indent=3)  # save our inside the file output
        array_of_movies = {'request': id_respose,
                           'data': lista_movies, 'size': size}

    return array_of_movies
