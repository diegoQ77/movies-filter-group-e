from src.dict_movie_list import built_array_movies


def release_date_of_movie(movie_name):
    order_movies_list = built_array_movies()
    data = []
    finded_movie = [
        movie for movie in order_movies_list if movie_name == movie['Title']]
    return finded_movie
