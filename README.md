# Movies filter

## About the system

You need to develop a command line utility that helps us query movie data.
Query response must be printed in the standard output, an also must generate
output file as detailed below

## Recommendations

- There are 5 groups of developers, each team has 4 developers that mus plan, write, test and demo this assignment
- This project must be demoed on Friday, October 7, 2022. (10 minutes top)
- Python version recommended 3.9.14 (if your team decides to use a different version, you need to research and fix any runtime issue because of libs incompatibility)

## Input files

The files this utility will be reading data from are:

    data
      |__movies.csv
      |__ratings.csv
      |__tags.csv

## Output files

The following files must be saved in the following file structure:

    output
        |__requests.csv
        |__0871f013c7f945e8a9d965775556e559.json
        |__abcd1f013c7f945e8a9d965775556e559.json

## Request.csv

The utility assigns a unique identifier for each request and appends request info in a requests.csv file (comma delimited file)

- First field is the request unique identifier,
- Second field contains the command name, arguments and values
- Third field is the datetime when we started processing the request

  2da609b2816d46ce9ee532d6c5cd39ca,"movies_explorer --title all --order desc --by title","2021-12-13 07:23:35"
  0871f013c7f945e8a9d965775556e559,"movies_explorer --title T\* --genre Animation","2021-12-20 07:23:35"
  dcae95b4a40348c1999edb37237f70af,"movies_explorer --title \"Home Alone\"","2021-12-23 07:23:35"

## Response

Each response is saved in a json file for example 0871f013c7f945e8a9d965775556e559.json

- where 0871f013c7f945e8a9d965775556e559 is a unique identifier for this request

## Getting started

**_We are the Team 5:_**

- _Gonzales de la Torre Ivan_
- _Morales Rivas Rodrigo_
- _Quisbert Gutierrez Diego_
- _Vasquez Rojas Brayan_

Flowchart : https://app.diagrams.net/#G1_4GrD2pF8k3dP1Jd-kuKhclByUUvbTcA

Our features are:

- Show the utility help
- List all movie ordered by [title] in [descendent] mode
- List all movies which title starts with T followed by any characters and have genre Animation
- List all release dates for movie title "Sabrina"
- List all tags for movie title "Home Alone 2"
- List all movies with rating of 3.0
- Count movie titles by genre
- Movies rated by user

## Show the utility help

The input for this feature must be in this format:

    python3 movies_explorer.py --help

This command should be print all the commands and his description.

## List all movie ordered by [title] in [descendent] mode

The input for this feature must be in this format:

    python3 movies_explorer.py --title all --order desc --by Title

This command should be print all the movies orderer in order ascendent or descendent.

    {
        "request_id": "0871f013c7f945e8a9d965775556e559",
        "data": [
            {
                "title": "ABCDEFG",
                "release_date": 1934,
                "genres": ["acd", "xyz", "fgh"]
            },
            {
                "title": "OPQRST",
                "release_date": 1950,
                "genres": ["acd"]
            }
        ],
        "size": 2
    }

## List all movies which title starts with T followed by any characters and have genre Animation

The input for this feature must be in this format:

    python3 movies_explorer.py --title T* --genre Animation

This command should be print all movies with initial letter **T** and with the genre **Animation**

    {
        "request_id": "1171f013c7f945e8a9d965775556e559",
        "data": [
            {
                "title": "Taaaaaa",
                "release_date": 1934,
                "genres": ["Animation"]
            },
            {
                "title": "Tipy",
                "release_date": 1970,
                "genres": ["Animation"]
            },
            {
                "title": "Ted",
                "release_date": 1980,
                "genres": ["Animation"]
            }
        ],
        "size": 3
    }

## List all release dates for movie title "Sabrina"

The input for this feature must be in this format:

    python3 movies_explorer.py --title Sabrina --date All

This command should be print all the movies with a specific **title** with all **release\_ date**

    {
        "request_id": "asss1f013c7f945e8a9d965775556e559",
        "data": [
            {
                "title": "Sabrina",
                "release_date": 1954,
                "genres": ["Comedy", "Romance"]
            },
            {
                "title": "Sabrina",
                "release_date": 1995,
                "genres": ["Comedy", "Romance"]
            }
        ],
        "size": 2
    }

## List all tags for movie title "Home Alone 2"

The input for this feature must be in this format:

    python3 movies_explorer.py --title "Home Alone" --tag all

This command should be print a movie with a specific **title** and show all the **tags** that it movie have

    {
        "request_id": "0871f013c7f945essss5775556e559",
        "data": [
            {
                "title": "Home Alone 2",
                "release_date": 1992,
                "genres": ["Children", "Comedy"],
                "tags": [
                    {"tag":"family", "date_time": "Monday, September 25, 2006 9:40:36 PM"},
                    {"tag":"funny", "date_time": "Monday, September 25, 2006 9:40:36 PM"},
                    {"tag":"Macaulay Culkin", "date_time": "Monday, September 25, 2006 9:40:36 PM"},
                    {"tag":"sequel", "date_time": "Monday, September 25, 2006 9:40:36 PM"}
                ]
            }
        ],
        "size": 1
    }

## List all movies with rating of 3.0

The input for this feature must be in this format:

    python3 movies_explorer.py --rating 3.0

This command should be print all movies with a specific **rating**.

    {
    "request_id": "abcd1f013c7f945e8a9d965775556e123",
    "data": [
    	{
    		"title": "Dummy title",
    		"release_date": 1971,
    		"genres": ["Documentary"],
    		"ratings": [
    		{
    			"date_time": "Monday, September 25, 2006 9:40:36 PM"
    			"rating": 3.0
    		}]
    	},
                       {
    		"title": "Some title",
    		"release_date": 1971,
    		"genres": ["Documentary"],
    		"ratings": [
    		{
    			"date_time": "Tuesday, September 26, 2006 9:40:36 PM"
    			"rating": 3.0
    		}]
    	}
    ],
    "size": 2

}

## Count movie titles by genre

The input for this feature must be in this format:

    python3 movies_explorer.py --genre all --counters 1 --order asc --by count

This command should be print all the **genres** with the **number of movies** with this genre assigned

    {
    "request_id": "0871f013c7f945e8a9d965775556e123",
    "data": [
                  { "genre": "Adventure", "count": 100 },
                  { "genre": "Animation", "count": 200 },
                  { "genre": "Children", "count": 345 },
                  { "genre": "Comedy", "count": 500 },
                { "genre": "Fantasy", "count": 567 } ],
    "size": 5

}

## Movies rated by user

The input for this feature must be in this format:

    python3 movies_explorer.py --rating all --user 3

This command should be print all **movies rated** for a specific **user**.

    {
    "request_id": "0871f013c7f945e8a9d965775556e123",
    "data": [
            {
                "title": "Documentary",
                "release_date": 1971,
                "genres": ["Documentary"],
                "ratings": [
                {
                    "date_time": "Monday, September 25, 2006 9:40:36 PM"
                    "rating": 4.5
                }]
            }
        ],
        "size": 1
    }
